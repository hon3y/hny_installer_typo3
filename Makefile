ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Help
#############################

help:
	bash help.sh $(ARGS)

###############################
# Install
###############################

install:
	bash install.sh $(ARGS)

###############################
# Deploy
###############################

deploy:
	bash deploy.sh $(ARGS)

#############################
# Argument fix workaround
#############################
%:
	@: